#!/usr/bin/env python3

from switchyard.lib.userlib import *

def mk_pkt(hwsrc, hwdst, ipsrc, ipdst, reply=False):
    ether = Ethernet(src=hwsrc, dst=hwdst, ethertype=EtherType.IP)
    ippkt = IPv4(src=ipsrc, dst=ipdst, protocol=IPProtocol.ICMP, ttl=32)
    icmppkt = ICMP()
    if reply:
        icmppkt.icmptype = ICMPType.EchoReply
    else:
        icmppkt.icmptype = ICMPType.EchoRequest
    return ether + ippkt + icmppkt

def hub_tests():
    s = TestScenario("hub tests")
    s.add_interface('eth0', '10:00:00:00:00:01')
    s.add_interface('eth1', '10:00:00:00:00:02')
    s.add_interface('eth2', '10:00:00:00:00:03')

    # test case 1: a frame with broadcast destination should get sent out
    # all ports except ingress
    testpkt = mk_pkt("30:00:00:00:00:02", "ff:ff:ff:ff:ff:ff", "172.16.42.2", "255.255.255.255")
    s.expect(PacketInputEvent("eth1", testpkt, display=Ethernet), "An Ethernet frame with a broadcast destination address should arrive on eth1")
    s.expect(PacketOutputEvent("eth0", testpkt, "eth2", testpkt, display=Ethernet), "The Ethernet frame with a broadcast destination address should be forwarded out ports e0 and e2")

    # test case 2: a frame with any unicast address except one assigned to hub
    # interface should be sent out the learned port.
    reqpkt = mk_pkt("20:00:00:00:00:01", "30:00:00:00:00:02", '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth0", reqpkt, display=Ethernet), "An Ethernet frame from 20:00:00:00:00:01 to 30:00:00:00:00:02 should arrive on eth0")
    s.expect(PacketOutputEvent("eth1", reqpkt, display=Ethernet), "Ethernet frame destined for 30:00:00:00:00:02 should be flooded out eth1")
    
    resppkt = mk_pkt("30:00:00:00:00:02", "20:00:00:00:00:01", '172.16.42.2', '192.168.1.100')
    s.expect(PacketInputEvent("eth1", resppkt, display=Ethernet), "An Ethernet frame from 30:00:00:00:00:02 to 20:00:00:00:00:01 should arrive on eth1")
    s.expect(PacketOutputEvent("eth0", resppkt, display=Ethernet), "Ethernet frame destined to 20:00:00:00:00:01 should be flooded out eth0.")    
    
    # test case 3: a frame with dest address of one of the interfaces on switch. POrts change.
    reqpkt = mk_pkt("20:00:00:00:00:01", "10:00:00:00:00:03", '192.168.1.100','172.16.42.2') 
    s.expect(PacketInputEvent("eth2", reqpkt, display=Ethernet), "An Ethernet frame should arrive on eth2 with destination address the same as eth2's MAC address")
    
    # FIXME it fails here. 
    # test case 4: flush ports with new dst and src addrs"
    x_pkt = mk_pkt("30:00:00:00:00:11", "30:00:00:00:00:99",  '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth0", x_pkt, display=Ethernet), "An Ethernet frame from 30:00:00:00:00:11 to 30:00:00:00:00:99 should arrive on eth0.")
    s.expect(PacketOutputEvent("eth1", x_pkt, "eth2", x_pkt, display=Ethernet), "Ethernet frame destined for 30:00:00:00:00:99 should be flooded out eth1 and eth2")

    x_pkt = mk_pkt("30:00:00:00:00:11", "10:00:00:00:00:03",  '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth0", x_pkt, display=Ethernet), "An Ethernet frame from 30:00:00:00:00:11 to switch at 10:00:00:00:00:03 should arrive on eth0.")
    
    x_pkt = mk_pkt("30:00:00:00:00:02", "12:00:00:00:00:03",  '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth2", x_pkt, display=Ethernet), "An ethernet fram from 30...02 to 12...03 should arrive on eth2.")
    s.expect(PacketOutputEvent("eth1", x_pkt, "eth0", x_pkt, display=Ethernet), "Ethernet frame destined for 12:...:03 should be flooded out ports eth1 and eth0")

    x_pkt = mk_pkt("20:00:00:00:00:01", "30:00:00:00:00:02",  '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth0", x_pkt, display=Ethernet), "An ethernet fram from 20...01 to 30...02 should arrive on eth0.")
    s.expect(PacketOutputEvent("eth2", x_pkt, display=Ethernet), "Ethernet frame from 30...2 should be sent out eth2")
    
    x_pkt = mk_pkt("20:00:00:00:00:01", "31:00:00:00:00:02",  '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth0", x_pkt, display=Ethernet), "An ethernet fram from 20...01 to 31...02 should arrive on eth0.")
    s.expect(PacketOutputEvent("eth2", x_pkt,"eth1", x_pkt, display=Ethernet), "Ethernet frame from 20:0001 should be flushed out eth2 and eth1")


    x_pkt = mk_pkt("29:00:00:00:00:01", "10:00:00:00:00:02",  '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth1", x_pkt, display=Ethernet), "An ethernet frame from 29:...:01 should arrive on eth1.")
    
    x_pkt = mk_pkt("28:00:00:00:00:01", "39:00:00:00:00:02",  '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth2", x_pkt, display=Ethernet), "An ethernet frame from 28...01 to 39...02 should arrive on eth2.")
    s.expect(PacketOutputEvent("eth0", x_pkt,"eth1", x_pkt, display=Ethernet), "Ethernet frame from 28:0001 should be flushed out eth0 and eth1")
    
    x_pkt = mk_pkt("19:00:00:00:00:01", "01:00:00:00:00:02", '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth0", x_pkt, display=Ethernet), "An ethernet fram from 19...01 to 01...02 should arrive on eth0.")
    s.expect(PacketOutputEvent("eth2", x_pkt,"eth1", x_pkt, display=Ethernet), "Ethernet frame from 19:01 should be flushed out eth2 and eth1")
    
    x_pkt = mk_pkt("18:00:00:00:00:01", '10:00:00:00:00:01', '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth0", x_pkt, display=Ethernet), "An ethernet fram from 18...01 to switch should arrive on eth0.")
    
    x_pkt = mk_pkt("16:00:00:00:00:21", "18:00:00:00:00:01", '192.168.1.100','172.16.42.2')
    s.expect(PacketInputEvent("eth0", x_pkt, display=Ethernet), "An ethernet fram from 16...21 to 18...01 should arrive on eth0.")
    s.expect(PacketOutputEvent("eth0", x_pkt, display=Ethernet), "Ethernet frame from 18:01 should be sent out eth0")

    return s
scenario = hub_tests()







