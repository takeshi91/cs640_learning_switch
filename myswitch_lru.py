# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

#!/usr/bin/env python3

from switchyard.lib.userlib import *
from collections import OrderedDict

# A class to represent the switch.
class Switch:
    def __init__(self, f_table, ports, macs, for_switch, id_num, ft_size):
        self.f_table = f_table      # The fowarding table of the switch
        self.ports = ports          # The ports of the switch
        self.macs = macs            # The addresses of the ports
        self.for_switch = for_switch # Boolean flag to check if packet headed for our switch.
        self.id_num = id_num        # The unique id number of switch
        self.ft_size = ft_size      # Maximum size of the forwarding table.

    # Adds (src, port) to the forwarding_table as MRU entry. 
    def update_table(self, src, in_port):
        print("Updating fucking table")
        self.f_table[src] = in_port
        self.f_table.move_to_end(src, last=False) 

    # Flushes packets from all ports in switch except the in_port.
    def flush_ports(self, net, in_port, packet):
        for pt in self.ports:
            if in_port != pt.name:
                log_debug("Flooding packet {} to {}".format(packet, pt.name))
                net.send_packet(pt.name, packet)  # packet is sent out from the pt. 

    def set_id_num (self, id_value):
        self.id_num = id_value
    
    def set_entry (self, addr, port):
        self.f_table[addr] = port

    def set_switch_flag (self, f_switch):
        self.for_switch = f_switch
    
    def get_id_num (self):
        return self.id_num

    def get_switch_flag (self):
        return self.for_switch
    
    def get_table (self):
        return self.f_table
    
    def get_max_size (self):
        return self.ft_size
    
    def remove_lru (self):
        print("what the fuck")
        return self.f_table.popitem(last=True)[0] # Remove LRU entry and return the src addr.

    # Checks if destination host exists as entry in forwarding table.
    def check_dest_entry(self, curr_port, input_port, packet, net, lru):
        if not(self.for_switch): 
            broadcast_addr = "ff:ff:ff:ff:ff:ff"
            if broadcast_addr == packet[0].dst:
                self.flush_ports(net, input_port, packet) 
            # If table full and removed LRU src is same as dst, it can be readded.
            elif curr_port != None and lru != packet[0].dst: 
                print("smdwamfc")
                self.update_table(packet[0].dst, curr_port)
                net.send_packet(curr_port, packet)
            else:  
                print("Flushing frame from ports.")
                self.flush_ports(net, input_port, packet)
                
    # TODO delete after finalizing. 
    def print_table(self):
        for k, v in self.f_table.items():
            print(k, v)
        print("\n")


def main(net):
    l_switch = Switch(OrderedDict(), net.interfaces(), [intf.ethaddr for intf in net.interfaces()], 0, 0, 5)
    while True:  # Continuously check for incoming packets, record them in forwarding table, and send them out. 
        try:    
            timestamp, input_port, packet = net.recv_packet()  # The respective port and data of this packet.  
        except NoPackets:
            print("NoPackets exception thrown in main function of myswitch_lru.py")
            continue
        except Shutdown:
            print("Shutdown exception thrown in main function of myswitch_lru.py")
            return
        
        l_switch.set_switch_flag(0)
        if packet[0].dst in l_switch.macs: # Update table but don't forward packet past your switch when dst = switch.
            print("switch received a packet meant for it.")
            l_switch.print_table() # TODO delete this when finalizing.  
            l_switch.set_switch_flag(1)
    
        f_table = l_switch.get_table()
        s_port = f_table.get(packet[0].src)     # Return the port of the respective existing address of src. 
        d_port = f_table.get(packet[0].dst)

        if len(l_switch.get_table()) == l_switch.get_max_size(): # Forwarding table is full and we need to make a new entry (no match for received packet):
            print("AT MAX CARRYING CAPACITY!!")
            lru = l_switch.remove_lru()
            l_switch.print_table()
            l_switch.update_table(packet[0].src, input_port)
            l_switch.print_table()
            l_switch.check_dest_entry(d_port, input_port, packet, net, lru)
            l_switch.print_table()
        
        elif s_port != None:  			   # If the source host of packet is an entry in our forwarding table:
            if s_port != input_port:
                l_switch.set_entry(packet[0].src, input_port)
            l_switch.check_dest_entry(d_port, input_port, packet, net, None)

        else:   				# Forwarding table is not full and we need to make new entry (no match for received packet):
            l_switch.update_table(packet[0].src, input_port)
            l_switch.check_dest_entry(d_port, input_port, packet, net, None)

        l_switch.print_table()
        net.shutdown()



