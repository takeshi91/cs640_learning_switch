# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

#!/usr/bin/env python3

from switchyard.lib.userlib import *
from collections import OrderedDict
from myswitch_lru import Switch
from spanningtreemessage import SpanningTreeMessage

# A class to represent the switch.
class Switch_STP:
    def __init__(self, Switch, SpanningTreeMessage, id_num, id_curr_root, num_hops, t_lastmsg, intf_lastmsg_rcv):
        self.Switch = Switch()      # the switch
        self.SpanningTreeMessage = SpanningTreeMessage(PacketHeaderBase)
        self.id_num = id_num        # The unique id number of switch
        self.t_lastmsg = t_lastmsg
        self.intf_lastmsg_rcv = intf_lastmsg_rcv
        self.id_curr_root = id_curr_root
        self.num_hops = num_hops

    # Flushes packets from all ports in switch except the in_port.
    def flush_ports(self, net, in_port, packet):
        for pt in self.ports:
            if in_port != pt.name:
                log_debug("Flooding packet {} to {}".format(packet, pt.name))
                net.send_packet(pt.name, packet)  # packet is sent out from the pt. 

    def set_id_num (self, id_value):
        self.id_num = id_value

    def get_id_num (self):
        return self.id_num

    def init_mode(self):
        for pt in self.ports
            pt.ifnum = 1

    def setFwrd(self, in_port):
        for pt in self.ports:
            if in_port == pt.name:
                pt.ifnum = 1

    def setFwrd(self, in_port):
        for pt in self.ports:
            if in_port == pt.name:
                pt.ifnum = 0

def main(net):
    l_switch = Switch(OrderedDict(), net.interfaces(), [intf.ethaddr for intf in net.interfaces()], 0, 5)
    spm = SpanningTreeMessage(PacketHeaderBase)
    my_stp = Switch_STP(l_switch, spm, min(l_switch.macs),min(l_switch.macs),0, 0, ports[0])
    #my_stp.spm.root()
    while True:  # Continuously check for incoming packets, record them in forwarding table, and send them out. 
        try:    
            timestamp, input_port, packet = net.recv_packet()  # The respective timestamp, port, and data of this packet.  
        except NoPackets:
            print("NoPackets exception thrown in main function of myswitch_lru.py")
            continue
        except Shutdown:
            print("Shutdown exception thrown in main function of myswitch_lru.py")
            return
        
        my_stp.Switch.set_switch_flag(0)
        if packet[0].root() < id_curr_root:
            id_curr_root = packet[0].root()
            packet[0].hops_to_root(packet[0].hops_to_root + 1);
            self.flush_ports(net, input_port, packet)
            if !input_port.ifnum:
                self.setFwrd(input_port)
            num_hops = packet[0].hops_to_root()
        elif packet[0].root() == id_curr_root:
            if packet[0].hops_to_root() + 1 < num_hops :
                if !input_port.ifnum:
                    self.setFwrd(input_port)
                packet[0].hops_to_root(packet[0].hops_to_root + 1);
                self.flush_ports(net, input_port, packet)
            elif packet[0].hops_to_root() == num_hops :
                if packet[0].hops_to_root() != packet[0].root().num_hops:
                    self.setBlk(input_port)
        if packet[0].dst in my_stp.Switch.macs: # Update table but don't forward packet past your switch when dst = switch.
            print("switch received a packet meant for it.")
            #my_stp.Switch.print_table() # TODO delete this when finalizing.  
            my_stp.Switch.set_switch_flag(1)
            


        f_table = my_stp.Switch.get_table()
        s_port = f_table.get(packet[0].src)     # Return the port of the respective existing address of src. 
        d_port = f_table.get(packet[0].dst)

        if s_port != None:  			   # If the source host of packet is an entry in our forwarding table:
            if s_port != input_port:
                my_stp.Switch.set_entry(packet[0].src, input_port)
            my_stp.Switch.check_dest_entry(d_port, input_port, packet, net)

        elif len(my_stp.Switch.get_table()) == my_stp.Switch.get_max_size(): # Forwarding table is full and we need to make a new entry (no match for received packet):
            my_stp.Switch.remove_lru()
            my_stp.Switch.update_table(packet[0].src, input_port)
            my_stp.Switch.check_dest_entry(d_port, input_port, packet, net)

        else:   				# Forwarding table is not full and we need to make new entry (no match for received packet):
            my_stp.Switch.update_table(packet[0].src, input_port)
            my_stp.Switch.check_dest_entry(d_port, input_port, packet, net)

        #my_stp.Switch.print_table()
        net.shutdown()



